<?php

$GLOBALS['TL_LANG']['tl_rocksolid_slide']['subline'][0] = 'Untertitel';
$GLOBALS['TL_LANG']['tl_rocksolid_slide']['subline'][1] = 'Geben Sie einen Untertitel an, wird er etwas kleiner unter dem Titel dieses Slides angezeigt.';

$GLOBALS['TL_LANG']['tl_rocksolid_slide']['link_to_page'][0] = 'Link auf Seite …';
$GLOBALS['TL_LANG']['tl_rocksolid_slide']['link_to_page'][1] = 'Verlinken Sie dieses Slide auf eine Seite.';