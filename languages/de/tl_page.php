<?php

$GLOBALS['TL_LANG']['tl_page']['hype_slider_options_legend'] = 'Slider Display-Optionen';
$GLOBALS['TL_LANG']['tl_page']['hype_slider_options'][0] = 'Header-Typ';
$GLOBALS['TL_LANG']['tl_page']['hype_slider_options'][1] = 'Wählen Sie den Header-Typ dieser Seite aus';
$GLOBALS['TL_LANG']['tl_page']['hype_slider_values']['rs_slider'] = 'Rocksolid-Slider';
$GLOBALS['TL_LANG']['tl_page']['hype_slider_values']['gmaps'] = 'Google Maps';
$GLOBALS['TL_LANG']['tl_page']['hype_slider_values']['static'] = 'Statisch (einzelnes Slide)';
$GLOBALS['TL_LANG']['tl_page']['hype_slider_values']['none'] = 'Kein Header';

$GLOBALS['TL_LANG']['tl_page']['hype_static_title'][0] = 'Statisch: Titel';
$GLOBALS['TL_LANG']['tl_page']['hype_static_title'][1] = 'Überschrift auf dem Hintergrundbild';

$GLOBALS['TL_LANG']['tl_page']['hype_static_subtitle'][0] = 'Statisch: Untertitel';
$GLOBALS['TL_LANG']['tl_page']['hype_static_subtitle'][1] = '(kleiner) Unterschrift des Titels';

$GLOBALS['TL_LANG']['tl_page']['hype_static_image'][0] = 'Statisch: Hintergrundbild';
$GLOBALS['TL_LANG']['tl_page']['hype_static_image'][1] = 'Hintergrundbild des statischen Headers';

$GLOBALS['TL_LANG']['tl_page']['hype_static_link'][0] = 'Link zu Seite …';
$GLOBALS['TL_LANG']['tl_page']['hype_static_link'][1] = 'Die Linkfläche verlinkt auf diese Seite';

$GLOBALS['TL_LANG']['tl_page']['hype_rs_slider'][0] = 'Rocksolid-Slider wählen';
$GLOBALS['TL_LANG']['tl_page']['hype_rs_slider'][1] = 'Sie können Ihre Slider unter "Rocksolid-Slider" links im Menü verwalten.';

$GLOBALS['TL_LANG']['tl_page']['hype_blocks_legend'] = 'Seitenelemente';
$GLOBALS['TL_LANG']['tl_page']['hype_blocks'] = ['Elemente wählen', 'Die Sortierung hat keinen Einfluss auf das Layout'];
$GLOBALS['TL_LANG']['tl_page']['hype_blocks_values']['category_icons'] = '"Ich suche" (Kategorie-Icons, oberhalb)';
$GLOBALS['TL_LANG']['tl_page']['hype_blocks_values']['brand_icons'] = 'Marken-Icons (unterhalb)';