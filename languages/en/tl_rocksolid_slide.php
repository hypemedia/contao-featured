<?php

$GLOBALS['TL_LANG']['tl_rocksolid_slide']['subline'][0] = 'Subtitle';
$GLOBALS['TL_LANG']['tl_rocksolid_slide']['subline'][1] = 'Provide a subtitle for this slide, it will be displayed in small under the title.';

$GLOBALS['TL_LANG']['tl_rocksolid_slide']['link_to_page'][0] = 'Link to page';
$GLOBALS['TL_LANG']['tl_rocksolid_slide']['link_to_page'][1] = 'Link this slide to a page.';