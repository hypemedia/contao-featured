<?php

$GLOBALS['TL_DCA']['tl_rocksolid_slide']['palettes']['default'] = str_replace(
    '{title_legend},title,',
    '{title_legend},title,subline,link_to_page,',
    $GLOBALS['TL_DCA']['tl_rocksolid_slide']['palettes']['default']);

$GLOBALS['TL_DCA']['tl_rocksolid_slide']['fields']['subline'] = array(
    'label'     => &$GLOBALS['TL_LANG']['tl_rocksolid_slide']['subline'],
    'exclude'   => true,
    'search'    => true,
    'flag'      => 1,
    'inputType' => 'text',
    'eval'      => array('maxlength' => 255),
    'sql'       => "varchar(255) NOT NULL default ''",
);

$GLOBALS['TL_DCA']['tl_rocksolid_slide']['fields']['link_to_page'] = array(
    'label'      => &$GLOBALS['TL_LANG']['tl_rocksolid_slide']['link_to_page'],
    'exclude'    => true,
    'inputType'  => 'pageTree',
    'foreignKey' => 'tl_page.title',
    'eval'       => array('fieldType' => 'radio', 'tl_class' => 'clr'),
    'sql'        => "int(10) unsigned NOT NULL default '0'",
    'relation'   => array('type' => 'hasOne', 'load' => 'eager')
);