<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 28/12/14
 * Time: 18:37
 */

/**
 * -------------------------------------------------------------------------
 * CLASSES
 * -------------------------------------------------------------------------
 */
ClassLoader::addClasses([
    'Hypemedia\\Contao\\Featured\\DCA\\TlPage'         => 'system/modules/hype-contao-featured/src/DCA/TlPage.php',
    'Hypemedia\\Contao\\Featured\\Inserttag\\Featured' => 'system/modules/hype-contao-featured/src/Inserttag/Featured.php',
]);

/**
 * -------------------------------------------------------------------------
 * TEMPLATES
 * -------------------------------------------------------------------------
 */
TemplateLoader::addFiles(array
(
    'hmf_rs_slider' => 'system/modules/hype-contao-featured/templates',
    'hmf_static'    => 'system/modules/hype-contao-featured/templates'
));