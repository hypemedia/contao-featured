<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 22/01/15
 * Time: 17:29
 */

namespace Hypemedia\Contao\Featured\Inserttag;

use Contao\FilesModel;
use Contao\FrontendTemplate;
use Contao\PageModel;

class Featured extends \Backend {

    public function replaceFeaturedTag($tag) {

        if($tag !== 'featured::render') {
            return '';
        }

        $page = $this->getPage();

        switch($page->hype_slider_options) {
            case 'rs_slider':
                return $this->generateSlider($page);
                break 1;
            case 'gmaps':
                return $this->generateGMaps($page);
                break 1;
            case 'static':
                return $this->generateStatic($page);
                break 1;
            case 'none':
            default:
                return '';
                break 1;
        }
    }

    private function generateSlider(PageModel $page) {

        $template = new FrontendTemplate('hmf_rs_slider');

        $paths = [];
        foreach($this->getRocksolidSlides($page->hype_rs_slider) as $slide) {
            $image = FilesModel::findByPk($slide['singleSRC']);
            $paths[] = $image->path;
        }
        $template->paths = $paths;

        return $template->parse();
    }

    private function generateGMaps(PageModel $page) {

        return '';
    }

    private function generateStatic(PageModel $page) {

        $template = new FrontendTemplate('hmf_rs_slider');
        $image = \FilesModel::findByPk($page->hype_static_image);
        $template->paths = [$image->path];

        return $template->parse();
    }

    private function getRocksolidSlides($sliderId) {

        return $this->Database->prepare('SELECT * FROM tl_rocksolid_slide WHERE pid=?')->execute($sliderId)->fetchAllAssoc();
    }

    private function getPage() {

        global $objPage;

        return $objPage;
    }
}