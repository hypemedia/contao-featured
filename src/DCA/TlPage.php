<?php namespace Hypemedia\Contao\Featured\DCA;

class TlPage extends \Backend {

    public function getSliderOptions() {

        return [
            'rs_slider' => &$GLOBALS['TL_LANG']['tl_page']['hype_slider_values']['rs_slider'],
            // 'gmaps' => &$GLOBALS['TL_LANG']['tl_page']['hype_slider_values']['gmaps'],
            'static'    => &$GLOBALS['TL_LANG']['tl_page']['hype_slider_values']['static'],
            'none'    => &$GLOBALS['TL_LANG']['tl_page']['hype_slider_values']['none'],
        ];
    }

    public function getRocksolidSliders() {

        $query = $this->Database->prepare('SELECT * FROM tl_rocksolid_slider')->execute();

        if($query->numRows < 1) {
            return [];
        }

        $return = [];

        while($query->next()) {
            $return[$query->id] = $query->name;
        }

        return $return;
    }
}